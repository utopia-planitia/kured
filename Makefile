
include ../kubernetes/etc/help.mk
include ../kubernetes/etc/cli.mk

.PHONY: deploy
deploy: kured.yaml ##@setup deploy to all nodes
	$(CLI) kubectl apply -f .

kured.yaml: Makefile
	curl --fail -o kured.yaml -L https://github.com/weaveworks/kured/releases/download/1.2.0/kured-1.2.0-dockerhub.yaml
